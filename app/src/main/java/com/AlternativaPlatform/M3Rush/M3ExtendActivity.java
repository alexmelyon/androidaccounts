package com.AlternativaPlatform.M3Rush;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.unity3d.player.UnityPlayer;

/**
 * Created by melekhin on 11.11.2016.
 */
//public class M3ExtendActivity extends UnityPlayerActivity implements GoogleApiClient.OnConnectionFailedListener {
public class M3ExtendActivity extends Activity implements GoogleApiClient.OnConnectionFailedListener {

    /**
     * http://console.developers.google.com/apis/credentials
     * Android OAuth client id
     */
    String ANDROID_OAUTH_CLIENT_ID = "695638652393-5kkc5547qp54alc965geks62ccft02dv.apps.googleusercontent.com";
    public static final int REQUEST_CODE_PICK_ACCOUNT = 9001;
    public static final int REQUEST_CODE_TOKEN_AUTH = 9002;
    public static final String UNITY_OBJECT_NAME = "ServiceObject";
    public static final String UNITY_METHOD_ON_AUTHORIZE_SUCCESS = "OnAuthorizeSuccess";
    public static final String UNITY_METHOD_ON_AUTHORIZE_FAILED = "OnAuthorizeFailed";
    public static final String UNITY_METHOD_ON_TOKEN_RECEIVED = "OnTokenReceived";

    private static M3ExtendActivity mContext;
    private static GoogleApiClient mGoogleApiClient;

    public M3ExtendActivity() {
        super();
        mContext = this;
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(ANDROID_OAUTH_CLIENT_ID)
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    /**
     * Method calls from Unity
     */
    public static void pickAccount() {
        Intent intent = AccountManager.newChooseAccountIntent(null, null, new String[]{"com.google"}, false, null, null, null, null);
        mContext.startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
    }


    /**
     * Method calls from Unity
     */
    public static void getToken() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        mContext.startActivityForResult(signInIntent, REQUEST_CODE_TOKEN_AUTH);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICK_ACCOUNT) {
            processPickAccount(requestCode, resultCode, data);
        } else if (requestCode == REQUEST_CODE_TOKEN_AUTH) {
            processGetToken(requestCode, resultCode, data);
        } else {
            UnityMessage(UNITY_METHOD_ON_AUTHORIZE_FAILED, "<Wrong request code " + requestCode + ">");
        }
    }

    void processGetToken(int requestCode, int resultCode, Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        if (data == null) {
            UnityMessage(UNITY_METHOD_ON_AUTHORIZE_FAILED, "<data is null>");
        } else if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String idToken = acct.getIdToken();
            UnityMessage(UNITY_METHOD_ON_TOKEN_RECEIVED, idToken);
        } else {
            UnityMessage(UNITY_METHOD_ON_AUTHORIZE_FAILED, "<Failed to get token>");
        }
    }

    void processPickAccount(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED) {
            UnityMessage(UNITY_METHOD_ON_AUTHORIZE_FAILED, "<result cancelled>");
        } else if (resultCode != RESULT_OK) {
            UnityMessage(UNITY_METHOD_ON_AUTHORIZE_FAILED, "<result " + resultCode + ">");
            return;
        } else if (data == null) {
            UnityMessage(UNITY_METHOD_ON_AUTHORIZE_FAILED, "<data is null>");
        } else {
            String email = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            if (email == null) {
                UnityMessage(UNITY_METHOD_ON_AUTHORIZE_FAILED, "<email is null>");
                return;
            }
            UnityMessage(UNITY_METHOD_ON_AUTHORIZE_SUCCESS, "" + email);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        UnityMessage(UNITY_METHOD_ON_AUTHORIZE_FAILED, "Connection failed: " + connectionResult);
    }

    void UnityMessage(final String methodName, final String data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    UnityPlayer.UnitySendMessage(UNITY_OBJECT_NAME, methodName, "" + data);
                } catch (Throwable e) {
                    Log.d("JCD", "UNITY METHOD " + methodName + "(" + data + ")");
                }
            }
        });
    }
}
