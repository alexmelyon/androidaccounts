//package com.AlternativaPlatform.M3Rush;
//
//import android.os.AsyncTask;
//import android.util.Log;
//
//import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
//import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
//import com.google.api.client.http.javanet.NetHttpTransport;
//import com.google.api.client.json.jackson2.JacksonFactory;
//
//import java.io.IOException;
//import java.security.GeneralSecurityException;
//import java.util.Arrays;
//
///**
// * Created by melekhin on 17.11.2016.
// */
//public class Verifier {
//
//    static final String CLIENT_ID = "695638652393-5kkc5547qp54alc965geks62ccft02dv.apps.googleusercontent.com";
//
//    public static void verify(final String token) {
//        final GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance())
//                .setAudience(Arrays.asList(CLIENT_ID))
//                .setIssuer("https://accounts.google.com")
//                .build();
//
//        new AsyncTask<Void, Void, Void>() {
//
//            @Override
//            protected Void doInBackground(Void... params) {
//
//                try {
//                    GoogleIdToken idToken = verifier.verify(token);
//                    if (idToken != null) {
//                        GoogleIdToken.Payload payload = idToken.getPayload();
//
//                        // Print user identifier
//                        String userId = payload.getSubject();
//                        Log.d("JCD", "User ID: " + userId);
//
//                        // Get profile information from payload
//                        String email = payload.getEmail();
//                        Log.d("JCD", "EMAIL " + email);
////                        Boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
////                        Log.d("JCD", "EMAIL VERIFIED " + emailVerified);
//                        String name = (String) payload.get("name");
//                        Log.d("JCD", "NAME " + name);
//                        String pictureUrl = (String) payload.get("picture");
//                        Log.d("JCD", "PICTURE " + pictureUrl);
//                        String locale = (String) payload.get("locale");
//                        Log.d("JCD", "LOCALE " + locale);
//                        String familyName = (String) payload.get("family_name");
//                        Log.d("JCD", "FAMILY NAME " + familyName);
//                        String givenName = (String) payload.get("given_name");
//                        Log.d("JCD", "GIVEN NAME " + givenName);
//
//                        // Use or store profile information
//                        // ...
//
//                    } else {
//                        System.out.println("Invalid ID token.");
//                    }
//                } catch (GeneralSecurityException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (IllegalStateException e) {
//                    e.printStackTrace();
//                }
//                return null;
//            }
//        }.execute();
//
//
//    }
//}
